export const colors = ["Red", "Yellow", "Green", "Blue"];

export const components = [
  { title: "Button" },
  { title: "Selection Control" },
  { title: "Input" },
  { title: "Snackbar" },
  { title: "Chips" },
  { title: "Progress Tabs" },
  { title: "Typography" },
  { title: "Card" },
  { title: "Pagination" },
  { title: "Progress Tabs" },
];
