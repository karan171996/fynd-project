import WatermarkComponent from "./containers/Watermark";
import HeadingComponent from "./components/Heading";
import DropDownComponent from "./components/DropDownSearch";
import { ColorProvider } from "./context";

function App() {
  return (
    <WatermarkComponent>
      <HeadingComponent>
        <b>Dropdown with search</b>
      </HeadingComponent>
      <ColorProvider>
        <DropDownComponent />
      </ColorProvider>
    </WatermarkComponent>
  );
}

export default App;
