import React, { useContext } from "react";
import { store } from "../../context";
import { Tag } from "antd";

export default function TagComponent() {
  const { state } = useContext(store);
  const activeData = state.activeState;
  let tagItem = [];
  state.initialStore.forEach((item) => {
    if (item.title === activeData) {
      tagItem = item.selectedItem;
    }
  });

  return tagItem.map((item, index) => {
    return <Tag key={index}>{item.title}</Tag>;
  });
}
