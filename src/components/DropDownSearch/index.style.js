import styled from "styled-components";

export default styled.div`
  .radio-group {
    display:flex;
    margin-top: 30px;
    margin-bottom: 30px;
  }
  .ant-select-selection-item {
    background: none;
    border: none;
  }
  .ant-select-selection-item-remove::after {
    content: "/";
  }
  .ant-select-selection-item-remove {
      display
  }
  .anticon anticon-close {
      display:none;
  }
`;
