import React, {
  useContext,
  useState,
  useEffect,
  useMemo,
  useCallback,
} from "react";
import { Select, Radio } from "antd";
import { store } from "../../context";
import CheckBoxComponent from "../CheckBox";
import TagComponent from "../TagComponents";
import StyleContainer from "./index.style";

export default function DropdownField() {
  const { state, dispatch } = useContext(store);
  const [selectOptions, setOptions] = useState([]);
  const [fiteredOption, setFilterOption] = useState([]);
  const [type, selectedComponent] = useState("Colors");
  useEffect(() => {
    setOptions(state.initialStore);
    const filterComponent = state.initialStore.filter(
      (item) => item.title === type
    );
    setFilterOption(filterComponent);
  }, [state.initialStore]);

  // Filtering the Options in DropDown while mainting the List from the root list(state.initialStore)
  const filterOptionHandler = (e) => {
    const filterComponent = state.initialStore.filter(
      (item) => item.title === type
    );
    const filterOptionData = filterComponent[0].data;
    const filterData = [];
    filterOptionData.forEach((item) => {
      if (item.title.toLowerCase().indexOf(e.toLowerCase()) >= 0) {
        filterData.push(item);
      }
    });

    let filterOptionNew = fiteredOption.map((item) => {
      return {
        ...item,
        data: filterData,
      };
    });
    setFilterOption(filterOptionNew);
  };

  // For Selecting the Single Component at a time
  const selectComponentHandler = (e) => {
    clearSelectedOptionHandler();
    selectedComponent(e.target.value);
    dispatch({
      type: "ACTIVE_STATE",
      payload: e.target.value,
    });
    const filterComponent = [];
    selectOptions.forEach((item) => {
      if (item.title === e.target.value) {
        filterComponent.push(item);
      }
    });
    setFilterOption(filterComponent);
  };

  const clearSelectedOptionHandler = () => {
    dispatch({
      type: "CLEAR_SELECTED",
      payload: {
        title: type,
      },
    });
  };

  const submitSelectedOptionHandler = () => {
    dispatch({
      type: "SUBMIT",
      payload: {
        title: type,
      },
    });
  };

  const checkValue = (type) => {
    let result = [];
    selectOptions.forEach((item) => {
      if (item.title === type) {
        result = item.selectedItem;
      }
    });
    return result;
  };

  const selectValues = useCallback(() => checkValue(type), []);
  const TagContainer = useMemo(() => TagComponent, []);
  return (
    <StyleContainer>
      <Radio.Group
        className="radio-group"
        onChange={selectComponentHandler}
        value={type}
      >
        <Radio value={"Colors"}>Colors</Radio>
        <Radio value={"Components"}>Components</Radio>
      </Radio.Group>
      <Select
        mode="multiple"
        showSearch
        style={{ width: 300 }}
        tagRender={TagContainer}
        suffixIcon={<span>{type}</span>}
        //   onChange={onChangeHandler}
        //   onFocus={onFocus}
        //   onBlur={onBlur}
        value={selectValues}
        onSearch={filterOptionHandler}
        maxTagCount="responsive"
        options={fiteredOption}
        dropdownRender={() => (
          <>
            {fiteredOption.map((item, index) => (
              <CheckBoxComponent selectData={item} />
            ))}
            {fiteredOption.map((item) => {
              if (item.indeterminate || item.checkedAll) {
                return (
                  <div className="bottom-section">
                    <span
                      className="clear-button"
                      onClick={clearSelectedOptionHandler}
                    >
                      Clear
                    </span>
                    <span
                      className="submit-button"
                      onClick={submitSelectedOptionHandler}
                    >
                      Submit
                    </span>
                  </div>
                );
              } else {
                return "";
              }
            })}
          </>
        )}
      />
    </StyleContainer>
  );
}
