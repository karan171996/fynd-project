import React, { useContext } from "react";
import { Checkbox } from "antd";
import { store } from "../../context";
import CheckBoxContainer from "./CheckBoxContainer";

export default function CheckBoxComponent({ selectData, unSetHandler }) {
  const { dispatch } = useContext(store);

  const onCheckAllChange = (e, key) => {
    dispatch({
      type: "SELECT_ALL",
      payload: {
        checked: e.target.checked,
        title: key,
      },
    });
  };

  return (
    <div>
      <Checkbox
        indeterminate={selectData.indeterminate}
        onChange={(e) => onCheckAllChange(e, selectData.title)}
        checked={selectData.checkedAll}
      />
      <CheckBoxContainer
        item={selectData.data}
        parentTitle={selectData.title}
      />
    </div>
  );
}
