import React, { useContext } from "react";
import { Checkbox } from "antd";
import { store } from "../../context";

export default function CheckBoxContainer({ item, parentTitle }) {
  const { dispatch } = useContext(store);

  const onChangeSingleHandler = (e, title) => {
    dispatch({
      type: "UPDATE_RESULT",
      payload: {
        checked: e.target.checked,
        title,
        parentTitle,
      },
    });
  };
  return (
    <div className="individual-list-section">
      {item.map((listItem) => {
        return (
          <Checkbox
            checked={listItem.checked}
            className="checkBoxBlock"
            onChange={(e) => onChangeSingleHandler(e, listItem.title)}
          >
            {listItem.title}
          </Checkbox>
        );
      })}
    </div>
  );
}
