import React from "react";

export default function HeadingComponent({ children }) {
  return <div>{children}</div>;
}
