import styled from "styled-components";

export const ContainerStyle = styled.div`
  background-color: #f8f8f8;
  padding: 50px 90px;
  width: 50%;
  margin-top: 10%;
  height: 500px;
`;
