import React from "react";
import { ContainerStyle } from "./index.style";

export default function WatermarkComponent({ children }) {
  return <ContainerStyle>{children}</ContainerStyle>;
}
