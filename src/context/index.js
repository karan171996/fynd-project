import React, { useReducer, createContext } from "react";
import { colors, components } from "../assets/data";
import { arrayToObject } from "../util/arrayToObjectMapper";
import functionDataMapper, {
  selectAllData,
  clearallData,
  addSelectedData,
} from "../util/dataMapper";

const initialState = {
  activeState: "Colors",
  initialStore: [
    {
      title: "Colors",
      data: [...arrayToObject(colors)],
      checkedAll: true,
      indeterminate: false,
      selectedItem: [],
    },
    {
      title: "Components",
      data: [...arrayToObject(components)],
      checkedAll: false,
      indeterminate: false,
      selectedItem: [],
    },
  ],
};

const store = createContext(initialState);
const { Provider } = store;

// function onlyUnique(value, index, self) {
//   return self.indexOf(value) === index;
// }

const ColorProvider = ({ children }) => {
  const [state, dispatch] = useReducer((state, action) => {
    switch (action.type) {
      case "ACTIVE_STATE":
        return {
          ...state,
          activeState: action.payload,
        };
      case "UPDATE_RESULT":
        return {
          ...state,
          initialStore: functionDataMapper(action.payload, state),
        };
      case "SELECT_ALL":
        return {
          ...state,
          initialStore: selectAllData(action.payload, state.initialStore),
        };
      case "CLEAR_SELECTED":
        return {
          ...state,
          initialStore: clearallData(action.payload, state.initialStore),
        };
      case "SUBMIT":
        return {
          ...state,
          initialStore: addSelectedData(action.payload, state.initialStore),
        };
      // case "PARENT_CHECKED":

      default:
        throw new Error();
    }
  }, initialState);

  return <Provider value={{ state, dispatch }}>{children}</Provider>;
};

export { store, ColorProvider };
