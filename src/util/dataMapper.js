export default function functionDataMapper(payload, originalState) {
  const mappingData = originalState.initialStore.map((item) => {
    if (item.title === payload.parentTitle) {
      const modifiedData = item.data.map((innerElement) => {
        if (innerElement.title === payload.title) {
          return {
            ...innerElement,
            checked: payload.checked,
          };
        } else {
          return innerElement;
        }
      });
      return {
        ...item,
        data: modifiedData,
      };
    } else {
      return item;
    }
  });
  return checkAllDataSelected(checkTagEmpty(mappingData));
}

export function checkAllDataSelected(mappedData) {
  const newFormedData = mappedData.map((item) => {
    const dataLength = item.data.length;
    let flag = false;
    let count = 0;
    item.data.forEach((item) => {
      if (item.checked) {
        count++;
        flag = true;
      } else {
        flag = false;
        return;
      }
    });
    if (flag && count === dataLength) {
      return {
        ...item,
        checkedAll: true,
        indeterminate: false,
      };
    } else if (!flag && count === 0) {
      return {
        ...item,
        indeterminate: false,
        checkedAll: false,
      };
    } else {
      return {
        ...item,
        indeterminate: true,
        checkedAll: false,
      };
    }
  });
  return newFormedData;
}

export function selectAllData(payload, originalData) {
  return originalData.map((item) => {
    if (item.title === payload.title) {
      const modifiedData = item.data.map((item) => {
        return {
          ...item,
          checked: payload.checked,
        };
      });
      return {
        ...item,
        data: modifiedData,
        indeterminate: false,
        checkedAll: payload.checked,
      };
    }
    return item;
  });
}

export function clearallData(payload, originalData) {
  return originalData.map((item) => {
    if (item.title === payload.title) {
      return {
        ...item,
        data: item.data.map((element) => {
          return {
            ...element,
            checked: false,
          };
        }),
        indeterminate: false,
        checkedAll: false,
        selectedItem: [],
      };
    }
    return item;
  });
}

export function addSelectedData(payload, originalData) {
  return originalData.map((item) => {
    if (payload.title === item.title) {
      const filterData = [];
      item.data.forEach((item) => {
        if (item.checked) filterData.push(item);
      });
      return {
        ...item,
        selectedItem: filterData,
      };
    }
    return item;
  });
}

export function checkTagEmpty(data) {
  return data.map((item) => {
    const checkedItem = item.data.filter((item) => item.checked);
    return {
      ...item,
      selectedItem: checkedItem.length > 0 ? item.selectedItem : [],
    };
  });
}
