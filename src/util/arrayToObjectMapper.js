export function arrayToObject(array) {
  return array.map((item) => {
    if (typeof item === "object") {
      return {
        ...item,
        checked: false,
      };
    } else {
      return {
        title: item,
        checked: true,
      };
    }
  });
}
